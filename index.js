const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const mongoose = require('mongoose')
const config = require('./config/config')
const todosRoute = require('./routes/todos')
const tagsRoute = require('./routes/tags')
const usersRoute = require('./routes/users')
const otpsRoute = require('./routes/otps')
const compression = require('compression')

const app = express()
const port = process.env.port || 3000

mongoose.set('strictQuery', false)

const connectToDatabase = async () => {
    try {
        await mongoose.connect(config.mongoDBPath)
    } catch (error) { }
}

connectToDatabase()

if (app.get('env') == 'development') {
    app.use(morgan('tiny'))
}
app.use(helmet())
app.use(compression())
app.use(express.json())
//app.use('/uploadsFiles', express.static(`${__dirname}/uploadsFiles`))
app.use('/api/v1', todosRoute)
app.use('/api/v1', tagsRoute)
app.use('/api/v1', usersRoute)
app.use('/api/v1', otpsRoute)
app.all('*', (req, res, next) => res.status(404).send({status: 'error', message: 'this api not found'}))

// app.post('/uploads',  multer().single('image'), async (req, res) => {
//     try {
//         console.log("FILE = ", req.file)
//         const response = await cloudinary.uploadImage(req.file.path)
//         console.log("RESPONSE == ", response)
//         res.send('DONE')
//     } catch(error) {
//         console.log("ERROR = ", error)
//         res.send('ERROR')
//     }
// })

app.listen(port, () => console.log("SERVER WORK FINE"))

module.exports = app;