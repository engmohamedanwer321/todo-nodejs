const { verifyToken } = require('../utils/token')

const user = (req, res, next) => {
    let token = req.headers.authorization
    if (token) {
        token = token.replace('Bearer ', '');
        try {
            req.user = verifyToken(token)
            next()
        } catch {
            res.status(501).send({ status: 'error', error: 'token is inccorect' })
        }
    } else {
        res.status(501).send({ status: 'error', error: 'access token not provided' })
    }
}

const admin = (req, res, next) => {
    let token = req.headers.authorization
    if (token) {
        token = token.replace('Bearer ', '');
        try {
            req.user = verifyToken(token)
            if(req.user.type != 'ADMIN') {
                res.status(501).send({ status: 'error', error: 'this user in not admin' })
            } else {
                next()
            }
        } catch {
            res.status(501).send({ status: 'error', error: 'token is inccorect' })
        }
    } else {
        res.status(501).send({ status: 'error', error: 'access token not provided' })
    }
}

module.exports = {
    user,
    admin
}