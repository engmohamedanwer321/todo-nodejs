const mongoose = require('mongoose')

const schema = new mongoose.Schema(
    {
        userId: { type:  mongoose.Schema.Types.ObjectId, ref: 'users', required: true},
        code: { type: String, required: true},
        expiredA: {type: Number, required: false, default: Date.now() + (60*1000*10)}
    },
    {
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
)

const otpsModel = mongoose.model('otps', schema)

module.exports = otpsModel