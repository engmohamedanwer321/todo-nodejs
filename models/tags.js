const mongoose = require('mongoose')

const schema = new mongoose.Schema(
    {
        name: { type: String, required: true, unique: true, minLength: 3, maxLength: 50 },
    },
    {
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
)

const tagsModel = mongoose.model('tags', schema)

module.exports = tagsModel
