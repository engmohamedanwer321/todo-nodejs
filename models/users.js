const mongoose = require('mongoose')

const schema = new mongoose.Schema(
    {
        avatar: { type: String, required: true },
        name: { type: String, required: true, minLength: 3, maxLength: 100 },
        email: { type: String, required: true, unique: true, minLength: 10, maxLength: 100 },
        password: { type: String, required: true, minLength: 8, maxLength: 1024 },
        isActive: { type: Boolean, required: false, default: false },
        type: { type: String, required: false, default: 'NORMAL', enum: ['NORMAL', 'ADMIN'] },
    },
    {
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
)

const usersModel = mongoose.model('users', schema)

module.exports = usersModel