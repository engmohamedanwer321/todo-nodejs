const mongoose = require('mongoose')

const schema = new mongoose.Schema(
    {
        title: { type: String, required: true, minLength: 3, maxLength: 50 },
        description: { type: String, required: true, minLength: 20, maxLength: 500 },
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
        isActive: { type: Boolean, default: true },
        tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'tags'}]
    },
    {
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
)

const todosModel = mongoose.model('todos', schema)

module.exports = todosModel