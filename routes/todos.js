const route = require('express').Router()
const todosModel = require('../models/todos')
const {createTodoValidation, updateTodoValidation} = require('../validations/todos')
const autherization = require('../midlewares/autherization')
const logger = require('../logger/winstonLogger')
const pagination = require('../utils/pagination')

route.get('/todos', autherization.user, async (req, res) => {
    try {
        const records = await todosModel.find(req.query).populate('tags').populate('user', '-password')
        res.send({ status: 'success', data: records })
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/todos/pagination', autherization.user, async (req, res) => {
    const { page = 1, limit = 10 } = req.query
    try {
        const records = await todosModel.find().limit(limit).skip((page - 1) * limit).populate('tags').populate('user', '-password')
        const count = await todosModel.count()
        const response = {
            status: 'success',
            data: {
                records: records,
                pagination: pagination.createPaginationObject(+page, +limit, count)
            }
        }
        res.send(response)
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/todos/myTodos', autherization.user, async (req, res) => {
    const { page = 1, limit = 10 } = req.query
    try {
        const records = await todosModel.find({user: req.user.id}).limit(limit).skip((page - 1) * limit).populate('tags').populate('user', '-password')
        const count = await todosModel.count({user: req.user.id})
        const response = {
            status: 'success',
            data: {
                records: records,
                pagination: pagination.createPaginationObject(+page, +limit, count)
            }
        }
        res.send(response)
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/todos/:id', autherization.user, async (req, res) => {
    try {
        const record = await todosModel.findById(req.params.id).populate('tags').populate('user', '-password')
        if (record) {
            res.send({ status: 'success', data: record })
        } else {
            res.status(501).send({ status: 'error', error: 'this todo not found' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.post('/todos', autherization.user, async (req, res) => {
    try {
        const { error, value } = createTodoValidation.validate(req.body);
        if (error) {
            res.status(501).send({ status: 'error', error: error })
        } else {
            value.user = req.user.id
            const record = await (await (await new todosModel(value).save()).populate('tags')).populate('user', '-password')
            res.send({ status: 'success', data: record })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.put('/todos/:id', autherization.user, async (req, res) => {
    try {
        const { error, value } = updateTodoValidation.validate(req.body);
        const record = await todosModel.findById(req.params.id)
        if(record) {
            if (error) {
                res.status(501).send({ status: 'error', error: error })
            } else {
                await todosModel.updateOne({ _id: req.params.id }, value)
                const record = await todosModel.findById(req.params.id).populate('tags')
                res.send({ status: 'success', data: record })
            }
        } else {
            res.status(501).send({ status: 'error', error: 'this todo not exits' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }

})

route.delete('/todos/:id', autherization.user, async (req, res) => {
    try {
        const record = await todosModel.findById(req.params.id)
        if (record) {
            await todosModel.deleteOne({_id: req.params.id})
            res.send({ status: 'success', data: 'todo removed successfully' })
        } else {
            res.status(501).send({ status: 'error', error: 'this todo not exits' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

module.exports = route