const route = require('express').Router()
const bcrypt = require('bcryptjs');
const lodash = require('lodash');
const usersModel = require('../models/users')
const otpsModel = require('../models/otps')
const { verifyAccountValidation, resendVerificationCodetValidation } = require('../validations/otps')
const { createToken } = require('../utils/token')
const logger = require('../logger/winstonLogger')
const email = require('../utils/email')
const otp = require('../utils/otp')


route.post('/otp/verify', async (req, res) => {
    try {
        const { error, value } = verifyAccountValidation.validate(req.body);
        if (error) {
            res.status(501).send({ status: 'error', error: error })
        } else {
            const record = await otpsModel.findOne({ userId: value.userId })
            if (record) {
                const isCodeCorrect = await bcrypt.compare(value.code, record.code)
                if (isCodeCorrect) {
                    if (record.expiredA < Date.now()) {
                        res.status(501).send({ status: 'error', error: 'code is expired' })
                    } else {
                        await otpsModel.deleteOne({userId: value.userId})
                        await usersModel.updateOne({_id: value.userId}, {isActive: true})
                        const user = await usersModel.findOne({_id: value.userId}).select('-password')
                        const userData = lodash.pick(user, ['_id', 'email', 'name', 'isActive', 'type', 'createdAt', 'updatedAt'])
                        userData.accesToken = createToken(user._id, user.type)
                        await email.sendMail(userData, `congrats ${userData.name} your account ativated and you can now enjoy`)
                        res.send({ status: 'success', data: userData })
                    }
                } else {
                    res.status(501).send({ status: 'error', error: 'code is incorrect' })
                }
            } else {
                res.status(501).send({ status: 'error', error: 'code is incorrect' })
            }
        }
    } catch (error) {
        console.log("ERROR = ", error)
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.post('/otp/resendVerifyCode', async (req, res) => {
    try {
        const { error, value } = resendVerificationCodetValidation.validate(req.body);
        if (error) {
            res.status(501).send({ status: 'error', error: error })
        } else {
            const otpCode = otp.generateOTP()
            const hashedOtpCode =  await bcrypt.hash(otpCode, 10)
            const record = await otpsModel.findOneAndUpdate({userId: value.userId}, {code: hashedOtpCode}).populate('userId', '-password')
            await email.sendMail(record.userId, `verfiy code of your account is ${otpCode}`)
            res.send({status: 'success', data: 'code is sent to your mail'})
        }
    } catch (error) {
        console.log("ERROR = ", error)
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

module.exports = route