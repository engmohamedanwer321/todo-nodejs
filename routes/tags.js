const route = require('express').Router()
const tagsModel = require('../models/tags')
const { tagValidation } = require('../validations/tags')
const autherization = require('../midlewares/autherization')
const logger = require('../logger/winstonLogger')
const pagination = require('../utils/pagination')

route.get('/tags', autherization.user, async (req, res) => {
    try {
        const records = await tagsModel.find(req.query)
        res.send({ status: 'success', data: records })
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/tags/pagination', autherization.user, async (req, res) => {
    const { page = 1, limit = 10 } = req.query
    try {
        const records = await tagsModel.find().limit(limit).skip((page - 1) * limit)
        const count = await tagsModel.count()
        const response = {
            status: 'success',
            data: {
                records: records,
                pagination: pagination.createPaginationObject(+page, +limit, count)
            }
        }
        res.send(response)
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/tags/:id', autherization.admin, async (req, res) => {
    try {
        const record = await tagsModel.findById(req.params.id).select('-password')
        if (record) {
            res.send({ status: 'success', data: record })
        } else {
            res.status(501).send({ status: 'error', error: 'this tag not found' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.post('/tags', autherization.admin, async (req, res) => {
    try {
        const { error, value } = tagValidation.validate(req.body);
        if (error) {
            res.status(501).send({ status: 'error', error: error })
        } else {
            const record = await new tagsModel(value).save()
            res.send({ status: 'success', data: record })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.put('/tags/:id', autherization.admin, async (req, res) => {
    try {
        const { error, value } = tagValidation.validate(req.body);
        const record = await tagsModel.findById(req.params.id)
        if (record) {
            if (error) {
                res.status(501).send({ status: 'error', error: error })
            } else {
                await tagsModel.updateOne({ _id: req.params.id }, value)
                const record = await tagsModel.findById(req.params.id)
                res.send({ status: 'success', data: record })
            }
        } else {
            res.status(501).send({ status: 'error', error: 'this tag not exits' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.delete('/tags/:id', autherization.admin, async (req, res) => {
    try {
        const record = await tagsModel.findById(req.params.id)
        if (record) {
            await tagsModel.deleteOnes({ _id: req.params.id })
            res.send({ status: 'success', data: 'tag removed successfully' })
        } else {
            res.status(501).send({ status: 'error', error: 'this tag not exits' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

module.exports = route