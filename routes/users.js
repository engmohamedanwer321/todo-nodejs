const route = require('express').Router()
const bcrypt = require('bcryptjs');
const lodash = require('lodash');
const usersModel = require('../models/users')
const otpsModel = require('../models/otps')
const { registerUserValidation, updateUserValidation } = require('../validations/users')
const {createToken} = require('../utils/token')
const autherization = require('../midlewares/autherization')
const logger = require('../logger/winstonLogger')
const pagination = require('../utils/pagination')
const email = require('../utils/email')
const otp = require('../utils/otp')
const multer = require('../utils/multer')
const cloudinary = require('../utils/cloudinary')
var path = require('path');


route.get('/users', autherization.admin, async (req, res) => {
    try {
        const records = await usersModel.find(req.query).select('-password')
        res.send({ status: 'success', data: records })
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.messagev })
    }
})

route.get('/users/pagination', autherization.admin, async (req, res) => {
    const { page = 1, limit = 10 } = req.query
    try {
        const records = await usersModel.find().limit(limit).skip((page - 1) * limit).select('-password')
        const count = await usersModel.count()
        const response = {
            status: 'success',
            data: {
                records: records,
                pagination: pagination.createPaginationObject(page, limit, count)
            }
        }
        res.send(response)
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.messagev })
    }
})

route.get('/users/profile', autherization.user, async (req, res) => {
    try {
        const record = await usersModel.findById(req.user.id).select('-password')
        if (record) {
            res.send({ status: 'success', data: record })
        } else {
            res.status(501).send({ status: 'error', error: 'this user not found' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.get('/users/:id', autherization.user, async (req, res) => {
    try {
        const record = await usersModel.findById(req.params.id).select('-password')
        if (record) {
            res.send({ status: 'success', data: record })
        } else {
            res.status(501).send({ status: 'error', error: 'this user not found' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.post('/users/register', multer().single('avatar'), async (req, res) => {
    try {
        const { error, value } = registerUserValidation.validate({...req.body, avatar: req.file.path});
        console.log(value)
        if (error) {
            res.status(501).send({ status: 'error', error: error })
        } else {
            const otpCode = otp.generateOTP()
            const hashedOtpCode =  await bcrypt.hash(otpCode, 10)
            value.password = await bcrypt.hash(value.password, 10)
            const avatar = await cloudinary.uploadFile(req.file.path)
            value.avatar = avatar.secure_url
            const record = await new usersModel(value).save()
            await new otpsModel({userId: record._id, code: hashedOtpCode}).save()
            const userData = lodash.pick(record, ['_id', 'avatar', 'email', 'name', 'isActive', 'type', 'createdAt', 'updatedAt'])
            const emailRes = await email.sendMail(userData, `verfiy code of your account is ${otpCode}`)
            console.log('emailRes == ', emailRes)
            res.send({ status: 'success', data: userData })
        }
    } catch (error) {
        console.log("ERROR = ", error)
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.post('/users/login', async (req, res) => {
    try {
        const record = await usersModel.findOne({ email: req.body.email })
        if (record) {
            const isPasswordCorrect = await bcrypt.compare(req.body.password, record.password)
            if (isPasswordCorrect) {
                const userData = lodash.pick(record, ['_id', 'avatar', 'email', 'name', 'isActive', 'type', 'createdAt', 'updatedAt'])
                if(userData.isActive) {
                    userData.accesToken = createToken(record._id, record.type)
                }
                res.send({ status: 'success', data: userData })
            } else {
                res.status(501).send({ status: 'error', error: 'the password and email is incorrect' })
            }
        } else {
            res.status(501).send({ status: 'error', error: 'the password and email is incorrect' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.put('/users', autherization.user, multer().single('avatar'), async (req, res) => {
    try {
        const { error, value } = updateUserValidation.validate(req.body);
        const record = await usersModel.findById(req.user.id)
        if (record) {
            if (error) {
                res.status(501).send({ status: 'error', error: error })
            } else {
                let avatar
                if(req.file) {
                    avatar = await cloudinary.uploadFile(req.file.path)
                    value.avatar = avatar.secure_url
                }
                await usersModel.updateOne({ _id: req.user.id }, value)
                const record = await usersModel.findById(req.user.id).select('-password')
                res.send({ status: 'success', data: record })
            }
        } else {
            res.status(501).send({ status: 'error', error: 'this user not exits' })
        }

    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

route.delete('/users/:id', autherization.admin, async (req, res) => {
    try {
        const record = await usersModel.findById(req.params.id)
        if (record) {
            await usersModel.deleteOne({ _id: req.params.id })
            res.send({ status: 'success', data: 'user deleted successfully' })
        } else {
            res.status(501).send({ status: 'error', error: 'this user not exits' })
        }
    } catch (error) {
        logger.error(error.message)
        res.status(501).send({ status: 'error', error: error.message })
    }
})

module.exports = route