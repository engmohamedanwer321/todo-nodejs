const Joi = require('joi');

const tagValidation = Joi.object({
    name: Joi.string().required().min(3).max(50).trim(),
})

module.exports = {
    tagValidation
}