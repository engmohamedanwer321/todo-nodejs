const Joi = require('joi');

const createTodoValidation = Joi.object({
    title: Joi.string().required().min(3).max(50).trim(),
    description: Joi.string().required().min(20).max(500).trim(),
    isActive: Joi.boolean().optional(),
    tags: Joi.array().optional(),
})

const updateTodoValidation = Joi.object({
    title: Joi.string().optional().min(3).max(50).trim(),
    description: Joi.string().optional().min(20).max(500).trim(),
    isActive: Joi.boolean().optional(),
    tags: Joi.array().optional(),
})

module.exports = {
    createTodoValidation,
    updateTodoValidation
}