const Joi = require('joi');

const verifyAccountValidation = Joi.object({
    userId: Joi.string().required(),
    code: Joi.string().required(),
})

const resendVerificationCodetValidation = Joi.object({
    userId: Joi.string().required(),
    email: Joi.string().required().email().trim(),
})

module.exports = {
    verifyAccountValidation,
    resendVerificationCodetValidation
}