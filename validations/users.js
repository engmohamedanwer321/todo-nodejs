const Joi = require('joi');

const registerUserValidation = Joi.object({
    avatar: Joi.string().required().pattern(/\.(jpg|jpeg|png|JPG|JEPG|PNG)$/),
    name: Joi.string().required().min(3).max(100).trim(),
    email: Joi.string().required().email().min(10).max(100).trim(),
    password: Joi.string().required().min(8).max(1024).trim(),
    type: Joi.string().optional().valid('NORMAL', 'ADMIN'),
})

const updateUserValidation = Joi.object({
    avatar: Joi.string().optional().pattern(/\.(jpg|png|JPG|PNG)$/),
    name: Joi.string().optional().min(3).max(100).trim(),
    email: Joi.string().optional().email().min(10).max(100).trim(),
})

module.exports = {
    registerUserValidation,
    updateUserValidation
}