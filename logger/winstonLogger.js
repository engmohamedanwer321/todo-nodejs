const winston = require('winston');
require('winston-mongodb');
const config = require('../config/config')

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({ format: "MMM-DD-YYYY HH:mm:ss" }),
    winston.format.json(),
    winston.format.prettyPrint()
  ),
  transports: [
    new winston.transports.File({ filename: 'logger/errors.log', level: 'error' }),
    new winston.transports.MongoDB({db: config.mongoDBPath, level: 'error', options: {useUnifiedTopology: true}})
  ],
});


module.exports = logger