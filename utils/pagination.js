

const createPaginationObject = (page, limit, count) => {
    return {
        currentPage: +page,
        //nextPage: recordsCount < limit ? +page : +page + 1,
        lastPage: Math.ceil(count / +limit),
        limit: +limit,
        recordsCount: count
    }
}


module.exports = {
    createPaginationObject
}