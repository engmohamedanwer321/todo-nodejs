const cloudinary = require('cloudinary').v2;

const uploadFile = async (imagePath) => {
    const options = {
        use_filename: true,
        unique_filename: false,
        overwrite: true,
    };
    return cloudinary.uploader.upload(imagePath, options);
};

module.exports = {
    uploadFile
}