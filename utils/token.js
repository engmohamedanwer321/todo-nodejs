const jwt = require('jsonwebtoken')

const createToken = (id, type) => {
    const token = jwt.sign({id: id, type: type}, 'JWT', {expiresIn: '1y'})
    return token
}

const verifyToken = (token) => {
    const decodedToken = jwt.verify(token, 'JWT')
    return decodedToken
}

module.exports = {
    createToken,
    verifyToken
}