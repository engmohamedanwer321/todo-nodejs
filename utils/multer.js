const { v4: uuidv4 } = require('uuid');
const multer = require('multer')
const path = require('path');
const fs = require('fs')


const multerUtil = () => {
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            const dest = path.join(__dirname, '../uploadsFiles')
            console.log(dest)
            fs.mkdir(dest, () => cb(null, dest))
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '_' + uuidv4() + '_' + file.originalname) 
        }
    })

    return multer({
        storage: storage
    });
}

module.exports = multerUtil